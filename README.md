# Dealership Application

## Application information

This application is built with spring boot, an in memory H2 database, an internal tomcat server and JavaFX. It´s running
as a standalone application and does not need additional software. The dependencies are handled with gradle and no
external jars are required.

## Database

The application connects to an in memory H2 database. This means all data are lost after the application is closed. The
advantage of this implementation is, that the application does not need an external database.

However, it is always possible to switch to an external database like MySQL.

It is possible to connect and manage the H2 database with this URL:<br/>
localhost:8080/h2-console/<br/>
Everything can be left as it is, except the field "JDCB URL". Please enter "jdbc:h2:mem:dealership" in there.

## Start project

Start the application using the class "DealershipApplication".
