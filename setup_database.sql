-- Create the new database
CREATE
DATABASE dealership;

-- Create the user for the database application
CREATE
USER 'dealership'@'%' IDENTIFIED BY 'dealership';

-- Grant privileges
GRANT ALL PRIVILEGES ON dealership.* TO
'dealership'@'%';
