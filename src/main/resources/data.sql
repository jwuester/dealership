INSERT INTO manufacturer (name, address)
VALUES ('BMW', '4250 W Henrietta Rd, Rochester, NY 14623');
INSERT INTO manufacturer (name, address)
VALUES ('General Motors Components Holdings, LLC', '1000 Lexington Ave, Rochester, NY 14606');
INSERT INTO manufacturer (name, address)
VALUES ('Audi Rochester', '3955 W Henrietta Rd, Rochester, NY 14623');

INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('Yellow with accident', '510Q', 2300, 1900, 1, true);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('2019 Black', '330i', 19500, 16000, 1, true);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('2020 Green', '230i', 15000, 11400, 1, false);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('X-Drive Black', 'X4', 34400, 24000, 1, false);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('Old white electric car', '230i', 8900, 6980, 1, false);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('Minivan black', 'Z4 M', 10500, 5400, 1, false);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('Blue Car', 'Q80', 60000, 40345, 2, false);
INSERT INTO model (name, series, list_price, buying_price, manufacturer_id, sold)
VALUES ('2019 Purple Old', 'A4', 15000, 11400, 3, false);

INSERT INTO buyer (lastname)
VALUES ('Smith');
INSERT INTO buyer (lastname)
VALUES ('Johnson');
INSERT INTO buyer (lastname)
VALUES ('Williams');
INSERT INTO buyer (lastname)
VALUES ('Brown');

INSERT INTO sale (date, price, buyer_id, model_id)
VALUES (CURDATE(), 10500, 1, 1);
INSERT INTO sale (date, price, buyer_id, model_id)
VALUES (CURDATE(), 12000, 2, 2);

INSERT INTO contact (firstname, lastname, e_mail, phone, birthdate, address, manufacturer_id)
VALUES ('Michael', 'Appell', 'MichaelSAppell@jourrapide.com', '765-251-0726', CURDATE(),
        '4759 Raintree Boulevard Marion, IN 46952', 1);
INSERT INTO contact (firstname, lastname, e_mail, phone, birthdate, address, manufacturer_id)
VALUES ('Keisha', 'Morley', 'KeishaJMorley@dayrep.com', '765-619-7548', CURDATE(),
        '3174 Capitol Avenue Anderson, IN 46016', 1);
INSERT INTO contact (firstname, lastname, e_mail, phone, birthdate, address, manufacturer_id)
VALUES ('Shirley', 'Woodard', 'ShirleyKWoodard@armyspy.com', '313-593-8717', CURDATE(),
        '1665 Tully Street Dearborn, MI 48126 ', 2);
INSERT INTO contact (firstname, lastname, e_mail, phone, birthdate, address, manufacturer_id)
VALUES ('Mary', 'Mercurio', 'MaryJMercurio@rhyta.com', '479-849-8451', CURDATE(),
        '1658 Cambridge Court Clarksville, AR 72830  ', 3);
