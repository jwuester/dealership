package edu.brockport.dealership;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EntityScan(basePackages = {"edu.brockport.dealership.entities"})
@EnableJpaRepositories(basePackages = {"edu.brockport.dealership.server.repositories"})
public class DealershipApplication {

    public static void main(String[] args) {
        Application.launch(GuiApplication.class, args);
    }

}
