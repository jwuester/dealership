package edu.brockport.dealership;


import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

public class GuiApplication extends Application {

    private ConfigurableApplicationContext applicationContext;
    protected StageManager stageManager;

    @Override
    public void init() {
        applicationContext = new SpringApplicationBuilder(DealershipApplication.class).run();
    }

    @Override
    public void start(Stage stage) {
        stageManager = applicationContext.getBean(StageManager.class, stage);
        displayInitialScene();
    }

    protected void displayInitialScene() {
        stageManager.switchScene(FxmlView.MENU);
    }

    @Override
    public void stop() {
        applicationContext.close();
        Platform.exit();
    }

}
