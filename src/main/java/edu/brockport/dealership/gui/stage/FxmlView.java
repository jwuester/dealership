package edu.brockport.dealership.gui.stage;

public enum FxmlView {

    MENU {
        @Override
        String getTitle() {
            return "Menu";
        }

        @Override
        String getFxmlFile() {
            return "/views/Menu.fxml";
        }
    }, MANUFACTURERS_OVERVIEW {
        @Override
        String getTitle() {
            return "Manufacturers";
        }

        @Override
        String getFxmlFile() {
            return "/views/ManufacturersOverview.fxml";
        }
    }, MANUFACTURERS_ADD {
        @Override
        String getTitle() {
            return "Add Manufacturer";
        }

        @Override
        String getFxmlFile() {
            return "/views/ManufacturerAdd.fxml";
        }
    }, MODELS_OVERVIEW {
        @Override
        String getTitle() {
            return "Models";
        }

        @Override
        String getFxmlFile() {
            return "/views/ModelsOverview.fxml";
        }
    }, CUSTOMER_CARS_OVERVIEW {
        @Override
        String getTitle() {
            return "Cars";
        }

        @Override
        String getFxmlFile() {
            return "/views/CustomerCarsOverview.fxml";
        }
    }, EMPLOYEE_MENU {
        @Override
        String getTitle() {
            return "Employee Menu";
        }

        @Override
        String getFxmlFile() {
            return "/views/EmployeeMenu.fxml";
        }
    }, MODEL_ADD {
        @Override
        String getTitle() {
            return "Add Model";
        }

        @Override
        String getFxmlFile() {
            return "/views/ModelAdd.fxml";
        }
    }, SALE {
        @Override
        String getTitle() {
            return "Sale";
        }

        @Override
        String getFxmlFile() {
            return "/views/Sale.fxml";
        }
    }, CONTACT_OVERVIEW {
        @Override
        String getTitle() {
            return "Contact";
        }

        @Override
        String getFxmlFile() {
            return "/views/ContactOverview.fxml";
        }
    }, CONTACT_ADD {
        @Override
        String getTitle() {
            return "Add Contact";
        }

        @Override
        String getFxmlFile() {
            return "/views/ContactAdd.fxml";
        }
    };

    abstract String getTitle();

    abstract String getFxmlFile();

}
