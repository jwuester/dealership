package edu.brockport.dealership.gui.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SaleModel {

    private String buyer;
    private String model;
    private Double price;
    private Date date;

}
