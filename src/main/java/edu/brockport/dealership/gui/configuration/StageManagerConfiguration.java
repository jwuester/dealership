package edu.brockport.dealership.gui.configuration;

import edu.brockport.dealership.gui.stage.SpringFXMLLoader;
import edu.brockport.dealership.gui.stage.StageManager;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class StageManagerConfiguration {

    private SpringFXMLLoader springFXMLLoader;

    @Autowired
    public StageManagerConfiguration(SpringFXMLLoader springFXMLLoader) {
        this.springFXMLLoader = springFXMLLoader;
    }

    @Bean
    @Lazy
    public StageManager stageManager(Stage stage) {
        return new StageManager(springFXMLLoader, stage);
    }

}
