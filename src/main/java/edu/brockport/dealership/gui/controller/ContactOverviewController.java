package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Contact;
import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ContactService;
import edu.brockport.dealership.server.services.ManufacturerService;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Component
public class ContactOverviewController implements FxmlController {

    public TableView<Contact> tblContact;
    public TableColumn<Contact, String> colFirstname;
    public TableColumn<Contact, String> colLastname;
    public TableColumn<Contact, String> colAddress;
    public TableColumn<Contact, String> colEMail;
    public TableColumn<Contact, String> colPhone;
    public TableColumn<Contact, Date> colBirthdate;

    public ComboBox<Manufacturer> comboManufacturer;

    private StageManager stageManager;
    private ContactService contactService;
    private ManufacturerService manufacturerService;
    private Manufacturer selectedItem;

    @Autowired
    @Lazy
    public ContactOverviewController(StageManager stageManager,
                                     ContactService contactService,
                                     ManufacturerService manufacturerService) {
        this.stageManager = stageManager;
        this.contactService = contactService;
        this.manufacturerService = manufacturerService;
    }

    @Override
    public void initialize() {
        colFirstname.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        colLastname.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        colAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        colEMail.setCellValueFactory(new PropertyValueFactory<>("eMail"));
        colPhone.setCellValueFactory(new PropertyValueFactory<>("phone"));
        colBirthdate.setCellValueFactory(new PropertyValueFactory<>("birthdate"));

        fillManufacturerCombo();
    }

    private void fillManufacturerCombo() {
        List<Manufacturer> all = this.manufacturerService.findAll();
        this.comboManufacturer.getItems().addAll(all);
    }

    private void fillTable() {
        List<Contact> contacts = this.contactService.findByManufacturerId(
                comboManufacturer.getSelectionModel().getSelectedItem().getId());
        tblContact.getItems().addAll(contacts);
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.EMPLOYEE_MENU);
    }

    public void onBtnAdd(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.CONTACT_ADD);
    }

    public void onComboManufacturer(ActionEvent actionEvent) {
        this.tblContact.getItems().clear();

        selectedItem = this.comboManufacturer.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            fillTable();
        }
    }
}
