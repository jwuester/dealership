package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import javafx.event.ActionEvent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class MenuController implements FxmlController {

    private StageManager stageManager;

    @Autowired
    @Lazy
    public MenuController(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void initialize() {
    }

    public void onBtnCustomer(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.CUSTOMER_CARS_OVERVIEW);
    }

    public void onBtnEmployee(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.EMPLOYEE_MENU);
    }

    public void onBtnManufacturer(ActionEvent actionEvent) {
        stageManager.switchScene(FxmlView.MANUFACTURERS_OVERVIEW);
    }
}
