package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ManufacturerService;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class ManufacturersAddController implements FxmlController {

    public TextField txtName;
    public TextField txtAddress;

    private StageManager stageManager;
    private ManufacturerService manufacturerService;

    @Autowired
    @Lazy
    public ManufacturersAddController(StageManager stageManager,
                                      ManufacturerService manufacturerService) {
        this.stageManager = stageManager;
        this.manufacturerService = manufacturerService;
    }

    @Override
    public void initialize() {
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MENU);
    }

    public void onBtnSave(ActionEvent actionEvent) {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName(txtName.getText());
        manufacturer.setAddress(txtAddress.getText());

        Manufacturer saved = manufacturerService.save(manufacturer);
        if (saved != null) {
            this.stageManager.switchScene(FxmlView.MANUFACTURERS_OVERVIEW);
        }
    }
}
