/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import javafx.event.ActionEvent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class EmployeeMenuController implements FxmlController {

    private StageManager stageManager;

    @Autowired
    @Lazy
    public EmployeeMenuController(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void initialize() {
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MENU);
    }

    public void onBtnSale(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.SALE);
    }

    public void onBtnManufacturerModel(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MANUFACTURERS_OVERVIEW);
    }

    public void onBtnContact(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.CONTACT_OVERVIEW);
    }
}
