package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Buyer;
import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.entities.Model;
import edu.brockport.dealership.entities.Sale;
import edu.brockport.dealership.gui.model.SaleModel;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.BuyerService;
import edu.brockport.dealership.server.services.ManufacturerService;
import edu.brockport.dealership.server.services.ModelService;
import edu.brockport.dealership.server.services.SaleService;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Component
public class SaleController implements FxmlController {

    public TextField txtPrice;
    public DatePicker dpDate;
    public ComboBox<Manufacturer> comboManufacturer;
    public ComboBox<Model> comboModel;
    public ComboBox<Buyer> comboBuyer;

    public TableColumn<SaleModel, String> colBuyer;
    public TableColumn<SaleModel, String> colModel;
    public TableColumn<SaleModel, Double> colPrice;
    public TableColumn<SaleModel, Date> colDate;
    public TableView<SaleModel> tblSales;

    private StageManager stageManager;
    private ManufacturerService manufacturerService;
    private ModelService modelService;
    private SaleService saleService;
    private BuyerService buyerService;

    @Autowired
    @Lazy
    public SaleController(StageManager stageManager,
                          ManufacturerService manufacturerService,
                          ModelService modelService,
                          SaleService saleService,
                          BuyerService buyerService) {
        this.stageManager = stageManager;
        this.manufacturerService = manufacturerService;
        this.modelService = modelService;
        this.saleService = saleService;
        this.buyerService = buyerService;
    }

    @Override
    public void initialize() {
        colBuyer.setCellValueFactory(new PropertyValueFactory<>("buyer"));
        colModel.setCellValueFactory(new PropertyValueFactory<>("model"));
        colPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        colDate.setCellValueFactory(new PropertyValueFactory<>("date"));

        fillSaleTable();
        fillManufacturerCombo();
        fillBuyerCombo();
    }

    private void fillBuyerCombo() {
        List<Buyer> all = this.buyerService.findAll();
        this.comboBuyer.getItems().addAll(all);
    }

    private void fillManufacturerCombo() {
        List<Manufacturer> all = this.manufacturerService.findAll();
        this.comboManufacturer.getItems().addAll(all);
    }

    private void fillSaleTable() {
        List<Sale> all = this.saleService.findAll();

        List<SaleModel> models = new ArrayList<>();
        for (Sale sale : all) {
            SaleModel saleModel = new SaleModel();
            saleModel.setModel(sale.getModel().getSeries());
            saleModel.setBuyer(sale.getBuyer().getLastname());
            saleModel.setPrice(sale.getPrice());
            saleModel.setDate(sale.getDate());

            models.add(saleModel);
        }

        this.tblSales.getItems().clear();
        this.tblSales.getItems().addAll(models);
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.EMPLOYEE_MENU);
    }

    public void btnOnSell(ActionEvent actionEvent) {
        Model selectedModel = comboModel.getSelectionModel().getSelectedItem();
        selectedModel.setSold(true);
        selectedModel = this.modelService.save(selectedModel);

        Sale sale = new Sale();

        try {
            sale.setPrice(Double.valueOf(txtPrice.getText()));
        } catch (NumberFormatException ignore) {
            sale.setPrice(0.0);
        }
        sale.setDate(convertToDateViaInstant(dpDate.getValue()));
        sale.setModel(selectedModel);
        sale.setBuyer(comboBuyer.getSelectionModel().getSelectedItem());

        this.saleService.save(sale);

        clearView();
        fillSaleTable();
    }

    private void clearView() {
        this.comboManufacturer.getSelectionModel().clearSelection();
        this.comboModel.getSelectionModel().clearSelection();
        this.comboBuyer.getSelectionModel().clearSelection();
        this.txtPrice.setText("");
        this.dpDate.setValue(null);
    }

    public void onComboManufacturer(ActionEvent actionEvent) {
        this.comboModel.getItems().clear();

        Manufacturer selectedItem = this.comboManufacturer.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            List<Model> models = this.modelService
                    .findByManufacturerIdAndSoldIsFalse(selectedItem.getId());
            this.comboModel.getItems().addAll(models);
        }
    }
}
