package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ManufacturerService;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
public class ManufacturersOverviewController implements FxmlController {

    public TableView<Manufacturer> tblManufacturer = new TableView<>();
    public TableColumn<Manufacturer, Integer> colId = new TableColumn<>();
    public TableColumn<Manufacturer, String> colName = new TableColumn<>();
    public TableColumn<Manufacturer, String> colAddress = new TableColumn<>();

    private StageManager stageManager;
    private ManufacturerService manufacturerService;

    private Manufacturer selectedManufacturer;

    @Autowired
    @Lazy
    public ManufacturersOverviewController(StageManager stageManager,
                                           ManufacturerService manufacturerService) {
        this.stageManager = stageManager;
        this.manufacturerService = manufacturerService;
    }

    @Override
    public void initialize() {
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colAddress.setCellValueFactory(new PropertyValueFactory<>("address"));

        fillTable();
    }

    private void fillTable() {
        List<Manufacturer> all = manufacturerService.findAll();
        tblManufacturer.getItems().addAll(all);
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.EMPLOYEE_MENU);
    }

    public void onBtnAdd(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MANUFACTURERS_ADD);
    }

    public void onTblManufacturer(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
            selectedManufacturer = this.tblManufacturer.getSelectionModel().getSelectedItem();
            if (selectedManufacturer != null) {
                this.stageManager.switchScene(FxmlView.MODELS_OVERVIEW);
            }
        }
    }
}
