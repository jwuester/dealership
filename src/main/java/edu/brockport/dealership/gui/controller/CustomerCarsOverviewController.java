package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.entities.Model;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ManufacturerService;
import edu.brockport.dealership.server.services.ModelService;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerCarsOverviewController implements FxmlController {


    public TableColumn<Model, String> colModelName;
    public TableColumn<Model, String> colSeries;
    public TableColumn<Model, Double> colListPrice;

    public TableColumn<Manufacturer, String> colManufacturerName;

    public TableView<Model> tblModels;
    public TableView<Manufacturer> tblManufacturers;

    private StageManager stageManager;
    private ManufacturerService manufacturerService;
    private ModelService modelService;

    @Autowired
    @Lazy
    public CustomerCarsOverviewController(StageManager stageManager,
                                          ManufacturerService manufacturerService,
                                          ModelService modelService) {
        this.stageManager = stageManager;
        this.manufacturerService = manufacturerService;
        this.modelService = modelService;
    }

    @Override
    public void initialize() {
        colManufacturerName.setCellValueFactory(new PropertyValueFactory<>("name"));

        colModelName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colSeries.setCellValueFactory(new PropertyValueFactory<>("series"));
        colListPrice.setCellValueFactory(new PropertyValueFactory<>("listPrice"));

        fillManufacturerTable();
    }

    private void fillManufacturerTable() {
        List<Manufacturer> all = this.manufacturerService.findAll();

        tblManufacturers.getItems().addAll(all);
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MENU);
    }

    public void onBtnBuy(ActionEvent actionEvent) {
    }

    public void onTblManufacturers(MouseEvent mouseEvent) {
        Manufacturer selectedItem = this.tblManufacturers.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            fillModelsTable(selectedItem.getId());
        }
    }

    private void fillModelsTable(Integer id) {
        List<Model> byManufacturerId = modelService.findByManufacturerIdAndSoldIsFalse(id);
        tblModels.getItems().clear();
        tblModels.getItems().addAll(byManufacturerId);
    }
}
