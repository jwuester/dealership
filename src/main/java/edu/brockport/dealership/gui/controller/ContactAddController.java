package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Contact;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ContactService;
import javafx.event.ActionEvent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class ContactAddController implements FxmlController {

    public TextField txtFirstname;
    public TextField txtLastname;
    public TextField txtAddress;
    public TextField txtEMail;
    public TextField txtPhone;
    public DatePicker dpBirthdate;

    private StageManager stageManager;
    private ContactOverviewController contactOverviewController;
    private ContactService contactService;

    @Autowired
    @Lazy
    public ContactAddController(StageManager stageManager,
                                ContactOverviewController contactOverviewController,
                                ContactService contactService) {
        this.stageManager = stageManager;
        this.contactOverviewController = contactOverviewController;
        this.contactService = contactService;
    }

    @Override
    public void initialize() {
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.CONTACT_OVERVIEW);
    }

    public void onBtnSave(ActionEvent actionEvent) {
        Contact contact = new Contact();
        contact.setManufacturer(contactOverviewController.getSelectedItem());
        contact.setFirstname(txtFirstname.getText());
        contact.setLastname(txtLastname.getText());
        contact.setEMail(txtEMail.getText());
        contact.setPhone(txtPhone.getText());
        contact.setAddress(txtAddress.getText());
        contact.setBirthdate(convertToDateViaInstant(dpBirthdate.getValue()));

        this.contactService.save(contact);

        onBtnBack(actionEvent);
    }
}
