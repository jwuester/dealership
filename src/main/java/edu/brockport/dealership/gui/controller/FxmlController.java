package edu.brockport.dealership.gui.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public interface FxmlController {

    void initialize();

    default Date convertToDateViaInstant(LocalDate dateToConvert) {
        if (dateToConvert != null) {
            return java.util.Date.from(dateToConvert.atStartOfDay()
                    .atZone(ZoneId.systemDefault())
                    .toInstant());
        }
        return null;
    }

}
