package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.entities.Model;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ModelService;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class ModelAddController implements FxmlController {

    public TextField txtName;
    public TextField txtSeries;
    public TextField txtBuyingPrice = new TextField();
    public TextField txtListPrice = new TextField();

    private StageManager stageManager;
    private ModelsOverviewController modelsOverviewController;
    private ModelService modelService;

    @Autowired
    @Lazy
    public ModelAddController(StageManager stageManager,
                              ModelsOverviewController modelsOverviewController,
                              ModelService modelService) {
        this.stageManager = stageManager;
        this.modelsOverviewController = modelsOverviewController;
        this.modelService = modelService;
    }

    @Override
    public void initialize() {
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MANUFACTURERS_OVERVIEW);
    }

    public void onBtnSave(ActionEvent actionEvent) {
        Model model = new Model();
        model.setName(txtName.getText());
        model.setSeries(txtSeries.getText());
        model.setSold(false);

        Manufacturer selectedManufacturer = modelsOverviewController.getManufacturersOverviewController()
                .getSelectedManufacturer();
        model.setManufacturer(selectedManufacturer);

        try {
            model.setBuyingPrice(Double.valueOf(txtBuyingPrice.getText()));
        } catch (NumberFormatException ignore) {
            model.setBuyingPrice(0.0);
        }
        try {
            model.setListPrice(Double.valueOf(txtListPrice.getText()));
        } catch (NumberFormatException ignore) {
            model.setListPrice(0.0);
        }

        Model saved = modelService.save(model);
        if (saved != null) {
            this.stageManager.switchScene(FxmlView.MODELS_OVERVIEW);
        }
    }
}
