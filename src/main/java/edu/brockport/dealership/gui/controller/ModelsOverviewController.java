package edu.brockport.dealership.gui.controller;

import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.entities.Model;
import edu.brockport.dealership.gui.stage.FxmlView;
import edu.brockport.dealership.gui.stage.StageManager;
import edu.brockport.dealership.server.services.ModelService;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
public class ModelsOverviewController implements FxmlController {

    public TableView tblModels;
    public TableColumn colId;
    public TableColumn colName;
    public TableColumn colListPrice;
    public TableColumn colBuyingPrice;
    public Label lblHeadline;

    private StageManager stageManager;
    private ManufacturersOverviewController manufacturersOverviewController;
    private ModelService modelService;

    @Autowired
    @Lazy
    public ModelsOverviewController(StageManager stageManager,
                                    ManufacturersOverviewController manufacturersOverviewController,
                                    ModelService modelService) {
        this.stageManager = stageManager;
        this.manufacturersOverviewController = manufacturersOverviewController;
        this.modelService = modelService;
    }

    @Override
    public void initialize() {
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colBuyingPrice.setCellValueFactory(new PropertyValueFactory<>("buyingPrice"));
        colListPrice.setCellValueFactory(new PropertyValueFactory<>("listPrice"));

        Manufacturer selectedManufacturer = manufacturersOverviewController.getSelectedManufacturer();
        lblHeadline.setText("Models of " + selectedManufacturer.getName());

        fillTable(selectedManufacturer);
    }

    private void fillTable(Manufacturer selectedManufacturer) {
        Integer id = selectedManufacturer.getId();
        List<Model> byManufacturerId = this.modelService.findByManufacturerIdAndSoldIsFalse(id);

        tblModels.getItems().addAll(byManufacturerId);
    }

    public void onBtnBack(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MANUFACTURERS_OVERVIEW);
    }

    public void onBtnAdd(ActionEvent actionEvent) {
        this.stageManager.switchScene(FxmlView.MODEL_ADD);
    }
}
