package edu.brockport.dealership.server.services;

import edu.brockport.dealership.entities.Model;
import edu.brockport.dealership.server.repositories.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ModelService {

    private ModelRepository repository;

    @Autowired
    public ModelService(ModelRepository repository) {
        this.repository = repository;
    }

    public List<Model> findByManufacturerIdAndSoldIsFalse(Integer id) {
        Iterable<Model> byManufacturerId = this.repository.findByManufacturerIdAndSoldIsFalse(id);
        return StreamSupport.stream(byManufacturerId.spliterator(), false)
                .collect(Collectors.toList());
    }

    public Model save(Model model) {
        return this.repository.save(model);
    }
}
