package edu.brockport.dealership.server.services;

import edu.brockport.dealership.entities.Sale;
import edu.brockport.dealership.server.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SaleService {

    private SaleRepository repository;

    @Autowired
    public SaleService(SaleRepository repository) {
        this.repository = repository;
    }

    public List<Sale> findAll() {
        Iterable<Sale> all = this.repository.findAll();
        return StreamSupport.stream(all.spliterator(), false).collect(Collectors.toList());
    }

    public Sale save(Sale sale) {
        return this.repository.save(sale);
    }
}
