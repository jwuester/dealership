package edu.brockport.dealership.server.services;

import edu.brockport.dealership.entities.Contact;
import edu.brockport.dealership.server.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ContactService {

    private ContactRepository repository;

    @Autowired
    public ContactService(ContactRepository repository) {
        this.repository = repository;
    }

    public List<Contact> findByManufacturerId(Integer id) {
        Iterable<Contact> byManufacturerId = this.repository.findByManufacturerId(id);
        return StreamSupport.stream(byManufacturerId.spliterator(), false)
                .collect(Collectors.toList());
    }

    public Contact save(Contact contact) {
        return this.repository.save(contact);
    }
}
