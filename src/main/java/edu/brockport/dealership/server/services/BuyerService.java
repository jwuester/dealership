package edu.brockport.dealership.server.services;

import edu.brockport.dealership.entities.Buyer;
import edu.brockport.dealership.server.repositories.BuyerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BuyerService {

    private BuyerRepository repository;

    @Autowired
    public BuyerService(BuyerRepository repository) {
        this.repository = repository;
    }

    public List<Buyer> findAll() {
        Iterable<Buyer> all = this.repository.findAll();
        return StreamSupport.stream(all.spliterator(), false).collect(Collectors.toList());
    }
}
