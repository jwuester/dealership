package edu.brockport.dealership.server.services;

import edu.brockport.dealership.entities.Manufacturer;
import edu.brockport.dealership.server.repositories.ManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ManufacturerService {

    private ManufacturerRepository repository;

    @Autowired
    public ManufacturerService(ManufacturerRepository repository) {
        this.repository = repository;
    }

    public List<Manufacturer> findAll() {
        Iterable<Manufacturer> all = this.repository.findAll();
        return StreamSupport.stream(all.spliterator(), false).collect(Collectors.toList());
    }

    public Manufacturer save(Manufacturer manufacturer) {
        return this.repository.save(manufacturer);
    }
}
