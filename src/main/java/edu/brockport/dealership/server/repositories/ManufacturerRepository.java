package edu.brockport.dealership.server.repositories;

import edu.brockport.dealership.entities.Manufacturer;
import org.springframework.data.repository.CrudRepository;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Integer> {
}
