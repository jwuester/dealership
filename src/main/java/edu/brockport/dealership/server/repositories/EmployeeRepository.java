package edu.brockport.dealership.server.repositories;

import edu.brockport.dealership.entities.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}
