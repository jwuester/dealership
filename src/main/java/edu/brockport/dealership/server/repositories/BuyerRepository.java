package edu.brockport.dealership.server.repositories;

import edu.brockport.dealership.entities.Buyer;
import org.springframework.data.repository.CrudRepository;

public interface BuyerRepository extends CrudRepository<Buyer, Integer> {
}
