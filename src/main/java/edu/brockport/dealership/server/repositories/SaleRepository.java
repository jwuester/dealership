package edu.brockport.dealership.server.repositories;

import edu.brockport.dealership.entities.Sale;
import org.springframework.data.repository.CrudRepository;

public interface SaleRepository extends CrudRepository<Sale, Integer> {
}
