package edu.brockport.dealership.server.repositories;

import edu.brockport.dealership.entities.Contact;
import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<Contact, Integer> {

    Iterable<Contact> findByManufacturerId(Integer id);

}
