package edu.brockport.dealership.server.repositories;

import edu.brockport.dealership.entities.Model;
import org.springframework.data.repository.CrudRepository;

public interface ModelRepository extends CrudRepository<Model, Integer> {

    Iterable<Model> findByManufacturerIdAndSoldIsFalse(Integer id);

}
