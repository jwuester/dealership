package edu.brockport.dealership.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class Buyer extends Person {

    @OneToMany(mappedBy = "buyer")
    private Set<Sale> sale;

    @Override
    public String toString() {
        return getLastname();
    }
}
