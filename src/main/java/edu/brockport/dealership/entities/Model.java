package edu.brockport.dealership.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class Model extends AbstractEntity {

    private String name;
    private String series;
    private Double listPrice;
    private Double buyingPrice;
    private boolean sold;
    @ManyToOne
    private Manufacturer manufacturer;
    @OneToOne(mappedBy = "model")
    private Sale sale;

    @Override
    public String toString() {
        return name;
    }
}
