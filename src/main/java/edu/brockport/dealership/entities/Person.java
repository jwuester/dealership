package edu.brockport.dealership.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@MappedSuperclass
public abstract class Person extends AbstractEntity {

    private String firstname;
    private String lastname;
    private String address;
    private String eMail;
    private String phone;
    private Date birthdate;

}
