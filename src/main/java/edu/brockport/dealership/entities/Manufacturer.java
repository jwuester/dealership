package edu.brockport.dealership.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class Manufacturer extends AbstractEntity {

    private String name;
    private String address;
    @OneToMany(mappedBy = "manufacturer")
    private Set<Model> models;
    @OneToMany(mappedBy = "manufacturer")
    private Set<Contact> contacts;

    @Override
    public String toString() {
        return name;
    }
}
