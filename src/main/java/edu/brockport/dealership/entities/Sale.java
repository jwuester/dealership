package edu.brockport.dealership.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity
public class Sale extends AbstractEntity {

    private Double price;
    private Date date;
    @ManyToOne
    private Buyer buyer;
    @ManyToOne
    private Employee employee;
    @OneToOne
    @JoinColumn(name = "model_id", referencedColumnName = "id")
    private Model model;

}
